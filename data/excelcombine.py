

'''

Class for emotional classification of a given query text
@author : debarghya nandi


import glob
import pandas as pd

path = "/home/goodwillhunting/Desktop/CLASSWORK/SEM2/Proje Sem 2/data/"
file_identifier = "*.xlsx"

all_data = pd.DataFrame()
for f in glob.glob(path + "/*" + file_identifier):
    df = pd.read_excel(f)
    all_data = all_data.append(df,ignore_index=True)
    '''
# A Dynamic Programming based Python Program for 0-1 Knapsack problem 
# Returns the maximum value that can be put in a knapsack of capacity W 
import numpy as np
def knapSack(W, wt, val, n): 
	K = [[0 for x in range(W+1)] for x in range(n+1)] 

	# Build table K[][] in bottom up manner 
	for i in range(n+1): 
		for w in range(W+1): 
			if i==0 or w==0: 
				K[i][w] = 0
			elif wt[i-1] <= w: 
				K[i][w] = max(val[i-1] + K[i-1][w-wt[i-1]], K[i-1][w]) 
			else: 
				K[i][w] = K[i-1][w] 
            

	return K

# Driver program to test above function 
val = [12,10,8,11,14,7,9] 
wt = [4,6,5,7,3,1,6] 
W =18
n = len(val) 
m=(knapSack(W, wt, val, n)) 
m=np.array(m)
l=m.T
# This code is contributed by Bhavya Jain 

# end of function knapSack 

# To test above function 
val = [12,10,8,11,14,7,9] 
wt = [4,6,5,7,3,1,6] 
W =18
n = len(val) 
print(knapSack(W , wt , val , n))