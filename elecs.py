from tkinter import *
from tkinter import messagebox
import time

root = Tk()
root.geometry("700x650")
root.title("INDIA ELECTIONS 2019")



def confirm_narendra():
    if messagebox.askyesno(title='Verify', message='Want to fetch Modi\'s tweets? '):
        time.sleep(4)
        messagebox.showinfo('Yes', '500 Modi tweets Collected')
        print("Narendra Modi tweets extracted!")
    else:
        messagebox.showinfo('No', 'Data collection cancelled')

    frame2.destroy()

def confirm_rahul():
    if messagebox.askyesno(title='Verify', message='Want to fetch Rahul\'s tweets? '):
        time.sleep(4)
        messagebox.showinfo('Yes', '500 Rahul tweets Collected')
        print("Rahul Gandhi tweets extracted!")
    else:
        messagebox.showinfo('No', 'Data collection cancelled')

    frame2.destroy()

def collec():
    global frame2
    frame2 = Toplevel(f2)

    frame2.title("Data Collection")
    frame2.geometry("300x250")



    Label(frame2, text="Whose information do you wish to Collect?").pack()


    modi_dataBtn = Button(frame2, text="Narendra Modi /BJP Party ",  width=40, height=3, command=confirm_narendra)
    modi_dataBtn.pack(fill=BOTH)


    Label(frame2, text='').pack()

    Button(frame2, text="Rahul Gandhi /Congress Party ", width=40, height=3, command=confirm_rahul).pack(fill=BOTH)


#####################################################################################
#                            SENTIMENTS                                             #
#####################################################################################

def narendraSentimentPic():
    # global f3
    screen3 = Toplevel(frame3)

    screen3.title("Narendra Modi sentiment")
    # screen3.geometry("700x700")

    explanation = """Over 4800 Narendra modi tweets were Collected 
                     over a span of 10 days ranging from
                     April 12,2019 to April 21,2019"""

    sent_results = """The Sentiment results are as follows:

                     Positive    3523
                     Negative    1281
                     """

    Label(screen3, text=explanation,  font="times 14", justify=LEFT).pack(fill=X)
    Label(screen3, text='', ).pack(fill=X)
    Label(screen3, text=sent_results, font="times 14", justify=LEFT).pack(fill=X)
    Label(screen3, text='').pack(fill=X)


    photo = PhotoImage(file="//home//goodwillhunting//Desktop//CLASSWORK//SEM2//Project Sem 2//visualizations//modiTweetSentiments.png")

    label = Label(screen3, text='NArendra modi sentiment', image=photo).pack()
    label.photo = photo


def rahulSentimentPic():

    screen3 = Toplevel(frame3)

    screen3.title("Rahul Gandhi sentiment")
    # screen3.geometry("700x700")

    explanation = """Over 4800 Rahul Gandhi tweets were Collected 
                     over a span of 10 days ranging from
                     April 12,2019 to April 21,2019"""

    sent_results = """The Sentiment results are as follows:

                     Positive    3402
                     Negative    1402
                     """

    Label(screen3, text=explanation,  font="times 14", justify=LEFT).pack(fill=X)
    Label(screen3, text='', ).pack(fill=X)
    Label(screen3, text=sent_results,  font="times 14", justify=LEFT).pack(fill=X)
    Label(screen3, text='').pack(fill=X)
    # logo = PhotoImage(file="//home//goodwillhunting//Downloads//a.gif")

    photo = PhotoImage(file="//home//goodwillhunting//Desktop//CLASSWORK//SEM2//Project Sem 2//visualizations//RahulTweetSentiments.png")

    label = Label(screen3, text='Rahul Gandhi sentiment', image=photo).pack()
    label.photo = photo


def bothSentimentPic():

    screen3 = Toplevel(frame3)

    screen3.title("Both Candidates sentiments")
    # screen3.geometry("700x700")

    explanation = """Over 9000 tweets from both Candidates were Collected 
                     over a span of 10 days ranging from
                     April 12,2019 to April 21,2019"""

    sent_results = """The Sentiment results are as follows:

                                         Modi 	Rahul 
                    Positive 	3523 	3402
                    Negative 	1281 	1402
                    """

    Label(screen3, text=explanation,  font="times 14", justify=LEFT).pack(fill=X)
    Label(screen3, text='',).pack(fill=X)
    Label(screen3, text=sent_results, font="times 14", justify=LEFT).pack(fill=X)
    Label(screen3, text='').pack(fill=X)
    # logo = PhotoImage(file="//home//goodwillhunting//Downloads//a.gif")

    photo = PhotoImage(file="//home//goodwillhunting//Desktop//CLASSWORK//SEM2//Project Sem 2//visualizations//BothTweetSentiments.png")



    label = Label(screen3, text='', image=photo).pack()
    label.photo = photo


def sentiment():
    global frame3
    frame3 = Toplevel(f3)
    frame3.title("Tweet sentiment")
    frame3.geometry("300x250")

    Label(frame3, text="Whose Tweet Sentiments do you wish to see?").pack()
    Label(frame3, text='').pack()

    Button(frame3, text="Narendra Modi /BJP Party ", width=40, height=3,command= narendraSentimentPic ).pack()

    Label(frame3, text='').pack()
    Button(frame3, text="Rahul Gandhi /Congress Party ", width=40, height=3, command=rahulSentimentPic).pack()

    Label(frame3, text='').pack()
    Button(frame3, text="Both Candidates", width=40, height=3, command=bothSentimentPic).pack()

#####################################################################################
#                            EMOTIONS                                               #
#####################################################################################


def narendraEmotionPic():
    # global screen5
    screen5 = Toplevel(frame4)

    screen5.title("Narendra Modi Emotion")
    # screen3.geometry("700x700")

    explanation = """Over 4800 Narendra modi tweets were Collected 
                     over a span of 10 days ranging from
                     April 12,2019 to April 21,2019"""

    sent_results = """The Emotion elicited are as follows:

                Joy         2879
                Surprise    1014
                Fear         444
                Sadness      265
                Trust        124
                Disgust       40
                Anger         28"""

    Label(screen5, text=explanation, font="times 14", justify=LEFT).pack(fill=X)
    Label(screen5, text='',).pack(fill=X)
    Label(screen5, text=sent_results, font="times 14", justify=LEFT).pack(fill=X)
    Label(screen5, text='').pack(fill=X)


    photo = PhotoImage(file="//home//goodwillhunting//Desktop//CLASSWORK//SEM2//Project Sem 2//visualizations//ModiEmotion.png")
    label = Label(screen5, text='Narendra Modi Emotion Analysis', image=photo).pack(side=LEFT)
    label.photo = photo




def rahulEmotionPic():
    # global screen6
    screen6 = Toplevel(frame4)

    screen6.title("Rahul Gandhi Emotion")
    # screen3.geometry("700x700")

    explanation = """Over 4800 Rahul Gandhi tweets were Collected 
                     over a span of 10 days ranging from
                     April 12,2019 to April 21,2019"""

    sent_results = """The Emotion elicited are as follows:

                Joy         2935
                Surprise     953
                Fear         656
                Sadness      165
                Disgust       40
                Anger         51"""

    Label(screen6, text=explanation, font="times 14", justify=LEFT).pack(fill=X)
    Label(screen6, text='',).pack(fill=X)
    Label(screen6, text=sent_results,font="times 14", justify=LEFT).pack(fill=X)
    Label(screen6, text='').pack(fill=X)
    # logo = PhotoImage(file="//home//goodwillhunting//Downloads//a.gif")

    photo = PhotoImage(file="//home//goodwillhunting//Desktop//CLASSWORK//SEM2//Project Sem 2//visualizations//RahulEmotion.png")
    label = Label(screen6, text='Rahul Gandhi Emotion Analysis', image=photo).pack(side=LEFT)
    label.photo = photo


def bothEmotionPic():
    # global screen7
    screen7 = Toplevel(frame4)

    screen7.title("Both Candidates sentiments")
    # screen3.geometry("700x700")

    explanation = """Over 9600 tweets from both Candidates were Collected 
                     over a span of 10 days ranging from
                    April 12,2019 to April 21,2019"""

    sent_results = """The Emotions elicited are as follows:

                                Modi 	Rahul 
                Joy 	2879 	2935
                Surprise 1014 	953
                Fear 	444 	656
                Sadness 	265 	165
                Disgust 	40 	40
                Anger 	28 	51"""

    Label(screen7, text=explanation,  font="times 14", justify=LEFT).pack(fill=X)
    Label(screen7, text='',).pack(fill=X)
    Label(screen7, text=sent_results,  font="times 14", justify=LEFT).pack(fill=X)
    Label(screen7, text='').pack(fill=X)
    # logo = PhotoImage(file="//home//goodwillhunting//Downloads//a.gif")

    photo = PhotoImage(file="//home//goodwillhunting//Desktop//CLASSWORK//SEM2//Project Sem 2//visualizations//BothEmotion.png")

    label = Label(screen7, text='', image=photo).pack()
    label.photo = photo



def emotion():
    global frame4
    frame4 = Toplevel(f4)
    frame4.title("Tweet sentiment")
    frame4.geometry("300x250")

    Label(frame4, text="Whose Tweet Emotions do you wish to see?").pack()
    Label(frame4, text='').pack()

    Button(frame4, text="Narendra Modi /BJP Party ", width=40, height=3,command= narendraEmotionPic ).pack()

    Label(frame4, text='').pack()
    Button(frame4, text="Rahul Gandhi /Congress Party ", width=40, height=3, command=rahulEmotionPic).pack()

    Label(frame4, text='').pack()
    Button(frame4, text="Both Candidates", width=40, height=3, command=bothEmotionPic).pack()

#####################################################################################
#                            ALGORITHMS                                             #
#####################################################################################
def confirm_logistic():
    messagebox.showinfo('Accuracy', 'Logistic Regression Accuracy: 0.789')
    frame4.destroy()
def logisticRegression():

    screen7 = Toplevel(frame4)

    screen7.title("Logistic Regression")
    # screen3.geometry("700x700")

    explanation = """{:^12}{:^12}{:^12}{:^12}{:^15}\n
                     {:^12}{:^12}{:^12}{:^12}{:^15}\n
                     {:^12}{:^12}{:^12}{:^12}{:^15}\n
                     
                     {:^12}{:^12}{:^12}{:^12}{:^15}\n
                     {:^12}{:^12}{:^12}{:^12}{:^15}\n
                     {:^12}{:^12}{:^12}{:^12}{:^15}\n
  
                    """.format('\t         type','precision','recall','f1-score','support',
                                                                '0', '0.92', '0.26', '0.40', '265',
                                                                '1','0.78','0.99','0.87','696',
                                                                'micro avg','0.79','0.79','0.79','961',
                                                                'macro avg','0.85','0.62','0.64','961',
                                                                'weighted avg','0.82','0.79','0.74','961' )

    Button(screen7, text="Accuracy", width=40, height=3, command=confirm_logistic).pack()
    Label(screen7, text='').pack()
    Label(screen7, text=explanation, font="times 14", justify=LEFT).pack(fill=X)
    Label(screen7, text='',).pack(fill=X)

def confirm_naive():
    messagebox.showinfo('Accuracy', 'Multinomiall Naive Bayes Accuracy: 0.740')
    frame4.destroy()

def naiveBayes():
    screen8 = Toplevel(frame4)

    screen8.title("Naive Bayes")
    # screen3.geometry("700x700")

    explanation = """{:^12}{:^12}{:^12}{:^12}{:^15}\n
                     {:^12}{:^12}{:^12}{:^12}{:^15}\n
                     {:^12}{:^12}{:^12}{:^12}{:^15}\n

                     {:^12}{:^12}{:^12}{:^12}{:^15}\n
                     {:^12}{:^12}{:^12}{:^12}{:^15}\n
                     {:^12}{:^12}{:^12}{:^12}{:^15}\n

                    """.format('\t         type', 'precision', 'recall', 'f1-score', 'support',
                               '0', '0.74', '0.26', '0.16', '265',
                               '1', '0.74', '0.99', '0.85', '696',
                               'micro avg', '0.74', '0.79', '0.79', '961',
                               'macro avg', '0.74', '0.62', '0.64', '961',
                               'weighted avg', '0.74', '0.79', '0.74', '961')

    Button(screen8, text="Accuracy", width=40, height=3, command=confirm_naive).pack()
    Label(screen8, text='').pack()
    Label(screen8, text=explanation, font="times 14", justify=LEFT).pack(fill=X)
    Label(screen8, text='', ).pack(fill=X)
def confirm_rfc():
    messagebox.showinfo('Accuracy', 'Random Forest Classifier Accuracy: 0.792')
    frame4.destroy()

def rfc():
    screen9 = Toplevel(frame4)

    screen9.title("Random Forest Classifier")
    # screen3.geometry("700x700")

    explanation = """{:^12}{:^12}{:^12}{:^12}{:^15}\n
                     {:^12}{:^12}{:^12}{:^12}{:^15}\n
                     {:^12}{:^12}{:^12}{:^12}{:^15}\n

                     {:^12}{:^12}{:^12}{:^12}{:^15}\n
                     {:^12}{:^12}{:^12}{:^12}{:^15}\n
                     {:^12}{:^12}{:^12}{:^12}{:^15}\n

                    """.format('\t         type', 'precision', 'recall', 'f1-score', 'support',
                               '0', '0.74', '0.26', '0.16', '265',
                               '1', '0.74', '0.99', '0.85', '696',
                               'micro avg', '0.74', '0.79', '0.79', '961',
                               'macro avg', '0.74', '0.62', '0.64', '961',
                               'weighted avg', '0.74', '0.79', '0.74', '961')

    Button(screen9, text="Accuracy", width=40, height=3, command=confirm_rfc).pack()
    Label(screen9, text='').pack()
    Label(screen9, text=explanation, font="times 14", justify=LEFT).pack(fill=X)
    Label(screen9, text='', ).pack(fill=X)

def confirm_vce():
    messagebox.showinfo('Accuracy', 'Voting Classifier Accuracy: 0.798')
    frame4.destroy()


def vce():
    screen11 = Toplevel(frame4)

    screen11.title("Voting Classifier")
    # screen3.geometry("700x700")

    explanation = """{:^12}{:^12}{:^12}{:^12}{:^15}\n
                     {:^12}{:^12}{:^12}{:^12}{:^15}\n
                     {:^12}{:^12}{:^12}{:^12}{:^15}\n

                     {:^12}{:^12}{:^12}{:^12}{:^15}\n
                     {:^12}{:^12}{:^12}{:^12}{:^15}\n
                     {:^12}{:^12}{:^12}{:^12}{:^15}\n

                    """.format('\t         type', 'precision', 'recall', 'f1-score', 'support',
                               '0', '0.88', '0.26', '0.26', '265',
                               '1', '0.78', '0.99', '0.95', '696',
                               'micro avg', '0.79', '0.79', '0.79', '961',
                               'macro avg', '0.83', '0.62', '0.64', '961',
                               'weighted avg', '0.81', '0.79', '0.74', '961')

    Button(screen11, text="Accuracy", width=40, height=3, command=confirm_vce).pack()
    Label(screen11, text='').pack()
    Label(screen11, text=explanation, font="times 14", justify=LEFT).pack(fill=X)
    Label(screen11, text='', ).pack(fill=X)

def algorithms():
    global frame4
    frame4 = Toplevel(f4)

    frame4.title("Algorithms")
    # frame4.geometry("700x250")



    Label(frame4, text="View information of the algorithms").pack()


    Button(frame4, text="Logistic Regression",  width=40, height=3, command=logisticRegression).pack(fill=BOTH)


    Label(frame4, text='').pack()

    Button(frame4, text="Naive Bayes ", width=40, height=3, command=naiveBayes).pack(fill=BOTH)

    Label(frame4, text='').pack()

    Button(frame4, text="Random Forest Classifier", width=40, height=3, command=rfc).pack(fill=BOTH)

    Label(frame4, text='').pack()

    Button(frame4, text="Voting Classifier Ensembler", width=40, height=3, command=vce).pack(fill=BOTH)

def results():

    # global f3
    screen10 = Toplevel(f5)

    screen10.title("Elections Opinion Poll")
    logo1 = PhotoImage(file="/home/goodwillhunting/Desktop/CLASSWORK/SEM2/Project Sem 2/visualizations/modi(1).gif")

    w2 = Label(screen10,text='Narendra Modi' ,image=logo1).pack()

    photo = PhotoImage(file="//home//goodwillhunting//Desktop//CLASSWORK//SEM2//Project Sem 2//visualizations//Results.png")

    label = Label(screen10, text='Results', image=photo).pack()
    label.photo = photo
#####################################################################################
#                            MAIN LAYOUT                                            #
#####################################################################################
f0 = Frame(root, bg='gray')
f0.pack(side=TOP, fill=BOTH,)

logo = PhotoImage(file="/home/goodwillhunting/Desktop/CLASSWORK/SEM2/Project Sem 2/visualizations/flag(1).png")

w1 = Label(f0, image=logo).pack(fill=BOTH,)
#
# logo1 = PhotoImage(file="/home/goodwillhunting/Downloads/modi(1).gif")
#
# w2 = Label(f0, image=logo1).pack(side=RIGHT)

# f1 = Frame(root, bg='gray')
# f1.pack(side=TOP, fill=BOTH, expand=YES, padx=5, pady=5)
#
# l1 = Label(f1, bg='steel blue', text='INDIA ELECTIONS 2019',font="Calibri55")
# l1.pack(side=LEFT, fill=BOTH, expand=YES, padx=5, pady=5)


f2 = Frame(root, bg='gray')
f2.pack(side=TOP, fill=BOTH, expand=YES, padx=5, pady=5)

data_collection_btn = Button(f2,text="Data Collection", anchor=CENTER, font="Calibri",command=collec)
data_collection_btn.pack(side=LEFT, fill=BOTH, expand=YES, padx=5, pady=5)


f3 = Frame(root, bg='gray')
f3.pack(side=TOP, fill=BOTH, expand=YES, padx=5, pady=5)

sentiment_btn = Button(f3,text="Tweets Sentiments", anchor=CENTER, font="Calibri",command=sentiment)
sentiment_btn.pack(side=LEFT, fill=BOTH, expand=YES, padx=5, pady=5)

emotion_btn = Button(f3,text="Tweets Emotions", anchor=CENTER, font="Calibri",command=emotion)
emotion_btn.pack(side=LEFT, fill=BOTH, expand=YES, padx=5, pady=5)


f4 = Frame(root, bg='gray')
f4.pack(side=TOP, fill=BOTH, expand=YES, padx=5, pady=5)

algo_btn = Button(f4,text="Algorithms", anchor=CENTER, font="Calibri",command=algorithms)
algo_btn.pack(side=LEFT, fill=BOTH, expand=YES, padx=5, pady=5)

# #
f5 = Frame(root, bg='gray')
f5.pack(side=TOP, fill=BOTH, expand=YES, padx=5, pady=5)

conclusion_btn = Button(f5,text="Results", anchor=CENTER, font="Calibri",command=results)
conclusion_btn.pack(side=LEFT, fill=BOTH, expand=YES, padx=5, pady=5)


mainloop()

