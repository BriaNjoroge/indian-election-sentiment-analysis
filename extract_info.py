#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 17 15:32:49 2019

@author: goodwillhunting
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import re,nltk,pickle

from nltk.corpus import stopwords
from sklearn.datasets import load_files
nltk.download('stopwords')
"""
#importing Datasets
reviews = load_files('/home/goodwillhunting/Anaconda Projects/Text Classification/txt_sentoken/')

X,y = reviews.data, reviews.target
"""
path = '/home/goodwillhunting/Desktop/CLASSWORK/SEM2/Project Sem 2/data/'

narendra_df = pd.read_csv(path+'nar.csv',)
print(narendra_df.shape)
narendra_df.info()
"""
# def slice_columns(narendra_df):
narendra_df = narendra_df.drop(['Date', 'Location',
                               'Author Statuses Count', 'Author Favourites Count',
                               'Author Friends Count', 'Author Followers Count', 
                                'Author Listed Count', 'Author Description', 'Selected (2)'],axis=1)

# narendra_df.columns
"""
narendra_df1 = narendra_df[['Author Name','Author','Content','pos',
       'neg', 'neu', 'compound', 'Emotion'   
       ]]

"""
for i in narendra_df.index.values:

    if narendra_df.loc[i,'compound']  >= 0.05 :
        narendra_df.loc[i,'sentiment'] = 'Positive'
    elif narendra_df.loc[i,'compound']  <= -0.05 :
        narendra_df.loc[i,'sentiment'] = 'Negative'
    else :
        narendra_df.loc[i,'sentiment'] = 'Neutral'
narendra_df.head()
"""

with open('/home/goodwillhunting/Anaconda Projects/Text Classification/tfidfmodel.pickle','rb') as f:
    vectorizer = pickle.load(f)
    
with open('/home/goodwillhunting/Anaconda Projects/Text Classification/classifier.pickle','rb') as f:
    clf = pickle.load(f)
    



    
total_pos = 0
total_neg = 0
list_tweets=[i for i in narendra_df.Content.values]
cleaned_content=[]
sentim=[]
    
for tweet in list_tweets:
    tweet = re.sub(r'^http://t.co/[a-zA-Z0-9]*\s', " ",str(tweet))
    tweet = re.sub(r'\s+http://t.co/[a-zA-Z0-9]*\s', " ",str(tweet))
    tweet = re.sub(r'\s+http://t.co/[a-zA-Z0-9]*$', " ",str(tweet))
    tweet = str(tweet).lower()
    tweet = re.sub(r"that's", "that is",str(tweet))
    
    
    tweet = re.sub(r'\W'," ",str(tweet))
    tweet = re.sub(r'\d', " ",str(tweet))
    tweet = re.sub(r'\s+[a-z]\s+'," ",str(tweet))
    tweet = re.sub(r'\s+[a-z]$'," ",str(tweet))
    tweet = re.sub(r'^[a-z]\s+'," ",str(tweet))
    tweet = re.sub(r'\s+'," ",str(tweet))
    
    cleaned_content.append(tweet)
    sent = clf.predict(vectorizer.transform([tweet]).toarray())
    sentim.append(sent)
"""
narendra_df['cleaned_content'] = cleaned_content
"""
#Bag of Words model
from sklearn.feature_extraction.text import CountVectorizer
vectorizer = CountVectorizer(max_features=len(X), min_df=3, max_df=0.6,stop_words = stopwords.words('english'))
X = vectorizer.fit_transform(cleaned_content).toarray()


#Term Frequency - Inverse Document Frequency (TF-IDF)
from sklearn.feature_extraction.text import TfidfTransformer
transformer = TfidfTransformer()
X = transformer.fit_transform(X).toarray()


#Bag of Words model
from sklearn.feature_extraction.text import TfidfVectorizer
vectorizer = TfidfVectorizer(max_features=len(X), min_df=3, max_df=0.6,stop_words = stopwords.words('english'))
X = vectorizer.fit_transform(cleaned_content).toarray()

y,_ = pd.factorize(narendra_df.sentiment)

#Training and test set
from sklearn.model_selection import train_test_split
text_train,text_test,sent_train,sent_test= train_test_split(X,y,test_size=0.2,random_state = 42)

from sklearn.linear_model import LogisticRegression
lg_clf = LogisticRegression()
lg_clf.fit(text_train,sent_train)
lg_clf.score(text_test,sent_test)

from sklearn.naive_bayes import MultinomialNB
nb_clf = MultinomialNB(alpha=1.0, fit_prior=True)
nb_clf.fit(text_train,sent_train)

nb_clf.score(text_test,sent_test)

from sklearn.ensemble import RandomForestClassifier
rand_clf=RandomForestClassifier(random_state=42,n_jobs=-1)
rand_clf.fit(text_train,sent_train)

rand_clf.score(text_test,sent_test)


from sklearn.naive_bayes import GaussianNB
nb_clf1 = GaussianNB()
nb_clf1.fit(text_train,sent_train)

nb_clf1.score(text_train,sent_train)

#from sklearn.svm import 
"""

objects = ['Positive','Negative']
y_pos =  np.arange(len(objects))

plt.bar(y_pos,[total_pos,total_neg],alpha=0.5)
plt.xticks(y_pos,objects)
plt.ylabel('Number')
plt.title('Number of Positive and Negative Tweets')
plt.show()

    """